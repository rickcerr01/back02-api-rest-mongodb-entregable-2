package com.practitioner.entregable2.controlador;

import com.practitioner.entregable2.modelo.Producto;
import com.practitioner.entregable2.servicio.impl.servicioProductoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apientregable/v2")
public class controladorProducto {

    @Autowired
    servicioProductoImpl servicioProducto;

    @GetMapping("/productos")
    public List<Producto> obtenerProductos(){
        return servicioProducto.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<Producto> obtenerProductoPorId(@PathVariable String id){
        return servicioProducto.findById(id);
    }

    @PostMapping("/productos")
    public Producto postProducto(@RequestBody Producto nuevoProducto){
        servicioProducto.save(nuevoProducto);
        return  nuevoProducto;
    }

    @PutMapping("/productos")
    public void actualizaProducto(@RequestBody Producto updateProducto){
        servicioProducto.save(updateProducto);
    }

    @DeleteMapping("/productos")
    public boolean borraProducto(@RequestBody Producto deleteProducto){
        return servicioProducto.delete(deleteProducto);
    }

}
