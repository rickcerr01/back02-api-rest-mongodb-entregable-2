package com.practitioner.entregable2.servicio.impl;

import com.practitioner.entregable2.modelo.Producto;
import com.practitioner.entregable2.servicio.repositoryProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class servicioProductoImpl {

    @Autowired
    repositoryProducto repositorioProducto;

    public List<Producto> findAll(){
        return repositorioProducto.findAll();
    }

    public Optional<Producto> findById(String id){
        return repositorioProducto.findById(id);
    }

    public Producto save(Producto entity){
        return repositorioProducto.save(entity);
    }

    public boolean delete(Producto entity){
        try{
            repositorioProducto.delete(entity);
            return true;
        } catch (Exception ex){
            return false;
        }
    }
}
