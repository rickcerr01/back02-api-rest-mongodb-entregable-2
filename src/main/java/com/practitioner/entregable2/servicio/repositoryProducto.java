package com.practitioner.entregable2.servicio;

import com.practitioner.entregable2.modelo.Producto;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface repositoryProducto extends MongoRepository<Producto, String> {
}
